#ifndef ARVOREB
#define ARVOREB
#define ORDEM_DA_ARVORE 5
#define true 1
#define false 0

/*typedef struct e{
	char pk[1024];
	int localRegistro,numElementos,nivel,pagina;
	struct e *prox,*menor,*maior;
}elementoB;*/

typedef struct e{
	char pk[ORDEM_DA_ARVORE-1][100];
	int prr[ORDEM_DA_ARVORE-1];
	struct e *filho[ORDEM_DA_ARVORE];
	int numElementos,numPonteiros,folha,numPagina;
}elementoB;

typedef struct a{
	int prr[ORDEM_DA_ARVORE];
	char pk[ORDEM_DA_ARVORE][100];
}armazenaRegistro;

#endif