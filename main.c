#include "main.h"

int main(int argc, char const *argv[]){
	int i=0;
	int opt=0;
	int numReg=0;
	char* nomeDoArquivo;
	elementoB *arvB;

	if(argc==3){
			

			if(strcmp("-r1",argv[2])==0){
				nomeDoArquivo = calloc( (strlen(argv[1])), sizeof(char));
				strcpy(nomeDoArquivo,argv[1]);
				
				if(verificarFixo(nomeDoArquivo)==-1){
					printf("O arquivo não possui registros de tamanho fixo\n");
					opt=5;

				} else {
					inicializarArvoreB(&arvB, nomeDoArquivo, &numReg);

					while(opt != 5){
						printf("\n\n====================================\n============Menu Inicial============\n====================================\n\n");
						printf("Selecione a opcao desejada\n");
						printf("1 - Buscar Registro\n");
						printf("2 - Inserir Registro\n");
						printf("3 - Mostrar Arvore-B\n");
						printf("4 - Remover Registro\n");
						printf("5 - Sair\n");
						scanf("%d",&opt);
						getchar();

						//printf("\n\n====================================\n====================================\n====================================\n\n");
						switch(opt){
							case 1:
								acharRegistro(&arvB,nomeDoArquivo);
								break;
							case 2:
								inserirRegistro(&arvB, nomeDoArquivo ,&numReg);
								break;
							case 3:
								printf("\n\n====================================\n==========Mostrar Arvore-B==========\n====================================\n\n");
								imprimirArvoreB(&arvB,0);
								break;
							case 4:
								//Remover Registro
								break;
							case 5:
								//sair
								break;
							default:
								opt=5;
						}
					}
				}
			}else if (strcmp("-r2",argv[2])==0){
				nomeDoArquivo = calloc( (strlen(argv[1])), sizeof(char));
				strcpy(nomeDoArquivo,argv[1]);

				if(verificarFixo(nomeDoArquivo)!=-1){
					printf("O arquivo não possui registros de tamanho variavel\n");
					opt=5;

				} else {
					inicializarArvoreBvariavel(&arvB, nomeDoArquivo, &numReg);

					while(opt != 5){
						printf("\n\n====================================\n============Menu Inicial============\n====================================\n\n");
						printf("Selecione a opcao desejada\n");
						printf("1 - Buscar Registro\n");
						printf("2 - Inserir Registro\n");
						printf("3 - Mostrar Arvore-B\n");
						printf("4 - Remover Registro\n");
						printf("5 - Sair\n");
						scanf("%d",&opt);
						getchar();

						switch(opt){
							case 1:
								acharRegistroVariavel(&arvB,nomeDoArquivo);
								break;
							case 2:
								inserirRegistroVariavel(&arvB, nomeDoArquivo ,&numReg);
								break;
							case 3:
								printf("\n\n====================================\n==========Mostrar Arvore-B==========\n====================================\n\n");
								imprimirArvoreB(&arvB,0);
								break;
							case 4:
								//Remover Registro
								break;
							case 5:
								//sair
								break;
							default:
								opt=5;
						}
					}
				}
			}else{
				printf("PARAMETRO INCORRETO:\n");
				printf("UTILIZE PARA REGISTROS FIXOS:nomedoexecutavel nomedoarquivo r1\n");
				printf("UTILIZE PARA REGISTROS VARIAVEIS:nomedoexecutavel nomedoarquivo r2\n");
				return 0;
			}
	} else {
		printf("ERRO AO PASSAR COM ARGUMENTOS\n");
		printf("UTILIZE PARA REGISTROS FIXOS:nomedoexecutavel nomedoarquivo r1\n");
		printf("UTILIZE PARA REGISTROS VARIAVEIS:nomedoexecutavel nomedoarquivo r2\n");
	}
	return 0;
}

