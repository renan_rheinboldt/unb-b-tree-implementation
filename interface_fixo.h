#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arvoreB.h"

int verificarFixo(char*);

void inicializarArvoreB(elementoB**, char*, int*);

void inserirNaPaginaVazia(elementoB**, char*, int*);

void inserirNaArvoreB(elementoB**, char*, int*);

void dividirPagina(elementoB**, int);

void criarArvoreB(elementoB**, char*, int*);

void imprimirArvoreB(elementoB **, int );

int inserirRegistro(elementoB **, char* ,int*);

int acharRegistro(elementoB **, char*);

void buscarRegistro(elementoB **, char*, int*, int*);