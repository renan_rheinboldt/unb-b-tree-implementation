#include "interface_fixo.h"

int verificarFixo(char* nomeArq){
	char leitor[1024];
	FILE* arq;
	int tamanhoRegistro,linha;

	arq = fopen(nomeArq, "r");

	//Verifica o tamanho do primeiro registro do arquivo
	fgets(leitor, 1024, arq);
	tamanhoRegistro = strlen(leitor);
	linha = 1;


	//Compara o primeiro registro com os outros, para verificar se possuem tamanho fixo
	while(fgets(leitor, 1024, arq)){
		linha++;
		if (strlen(leitor) != tamanhoRegistro){
			/* O registro não é fixo */
			printf("ERRO LINHA %d \n", linha);
			fclose(arq);
			return -1;
		}
	}
	fclose(arq);

	return tamanhoRegistro;
}

void inicializarArvoreB(elementoB **arvB, char* nomeArq, int *numRegistro){
	FILE *arq;
	elementoB *aux;
	char leitor[ORDEM_DA_ARVORE];
	int tamanhoRegistro,i,j;

	tamanhoRegistro = verificarFixo(nomeArq);

	arq=fopen(nomeArq,"r");

	/*Iicializando Árvore-B*/
	fgets(leitor,ORDEM_DA_ARVORE,arq);
	criarArvoreB(arvB,leitor,numRegistro);
	fseek(arq, 119*sizeof(char) , SEEK_CUR);
	
	//Inserindo outros elementos na ÁrvoreB
	
	while(fgets(leitor,ORDEM_DA_ARVORE,arq)){
		*numRegistro += 1;
		inserirNaArvoreB(arvB, leitor, numRegistro);
		fseek(arq, 119*sizeof(char) , SEEK_CUR);
	}

	fclose(arq);
}

void criarArvoreB(elementoB **arvB, char* elemento, int* prr){
	*arvB = (elementoB*) malloc(sizeof(elementoB));
	(*arvB)->folha = true;
	(*arvB)->numElementos = 1;
	(*arvB)->prr[0] = *prr;
	(*arvB)->numPagina = 0;
	strcpy( ((*arvB)->pk[0]), elemento);
}

void dividirPagina(elementoB **arvB, int i){
	elementoB *y,*z;
	int j;

	z = (elementoB*) malloc(sizeof(elementoB));
	y = (*arvB)->filho[i];
	z->folha = y->folha;
	z->numElementos = (ORDEM_DA_ARVORE/2)-1;
	z->numPagina = y->numPagina+2;
	(*arvB)->numPagina = y->numPagina;
	y->numPagina += 1;

	for (j = 1; j < (ORDEM_DA_ARVORE/2); ++j){

		z->prr[j-1] = y->prr[j+(ORDEM_DA_ARVORE/2)]; 
		strcpy(z->pk[j-1],y->pk[j+(ORDEM_DA_ARVORE/2)]);
	}
	if (y->folha==false){
		for (j = 1; j < (ORDEM_DA_ARVORE/2)+1; ++j){
			z->filho[j-1] = y->filho[j+(ORDEM_DA_ARVORE/2)];
		}
	}
	y->numElementos = (ORDEM_DA_ARVORE/2);
	for (j = (*arvB)->numElementos; j > i; --j){
		(*arvB)->filho[j+1] = (*arvB)->filho[j];
	}
	(*arvB)->filho[i+1] = z;
	for (j = (*arvB)->numElementos; j > i; --j){
		(*arvB)->prr[j]=(*arvB)->prr[j-1];
		strcpy((*arvB)->pk[j],(*arvB)->pk[j-1]);
	}
	(*arvB)->prr[i] = y->prr[ORDEM_DA_ARVORE/2];
	strcpy((*arvB)->pk[i],y->pk[ORDEM_DA_ARVORE/2]);
	(*arvB)->numElementos += 1;
}

void inserirNaArvoreB(elementoB **arvB, char* elemento, int* reg){
	elementoB *aux,*novaRaiz;

	aux = *arvB;

	if (aux->numElementos==ORDEM_DA_ARVORE-1){
		novaRaiz = (elementoB*) malloc(sizeof(elementoB));
		*arvB = novaRaiz;
		novaRaiz->folha = false;
		novaRaiz->numElementos = 0;
		novaRaiz->filho[0] = aux;
		dividirPagina(&novaRaiz,0);
		inserirNaPaginaVazia(&novaRaiz,elemento,reg);
	} else {
		inserirNaPaginaVazia(&aux,elemento,reg);
	}
}

void inserirNaPaginaVazia(elementoB** arvB, char* elemento, int* reg){
	int i;

	i = (*arvB)->numElementos-1;
	
	if ((*arvB)->folha==true){
		while( (i >= 0) && (strcmp(elemento,(*arvB)->pk[i])<0 ) ){
			(*arvB)->prr[i+1]=(*arvB)->prr[i];
			strcpy((*arvB)->pk[i+1],(*arvB)->pk[i]);
			i--;
		}
		(*arvB)->prr[i+1]=*reg;
		strcpy((*arvB)->pk[i+1],elemento);

		(*arvB)->numElementos += 1;

	} else {

		while( (i >= 0) && (strcmp(elemento,(*arvB)->pk[i])<0)){
			i--;
		}
		i++;
		
		if((*arvB)->filho[i]->numElementos == ORDEM_DA_ARVORE-1 ){
			dividirPagina(arvB,i);
			if (strcmp(elemento,(*arvB)->pk[i])>0){
				i++;
			}
		}

		inserirNaPaginaVazia(&((*arvB)->filho[i]),elemento,reg);
	}
}

void imprimirArvoreB(elementoB **arvB, int nivel){
	int i;

	
	if ((*arvB)->folha==false){
		printf("N%d  P%d |",nivel,(*arvB)->numPagina);
		for (i = 0; i < (*arvB)->numElementos; ++i){
			printf("%s|", (*arvB)->pk[i]);
		}
		printf("  |");
		for (i = 0; i < ((*arvB)->numElementos)+1; ++i){
			printf("%d|",(*arvB)->filho[i]->numPagina);
		}
		printf("\n");
		nivel++;
		for (i = 0; i < ((*arvB)->numElementos)+1; ++i){
			imprimirArvoreB(&((*arvB)->filho[i]),nivel );
		}
	} else {
		printf("N%d  P%d |",nivel,(*arvB)->numPagina);
		for (i = 0; i < (*arvB)->numElementos; ++i){
			printf("%s|", (*arvB)->pk[i]);
		}
		printf("  |");
		for (i = 0; i < ((*arvB)->numElementos)+1; ++i){
			printf("-|");
		}
		printf("\n");
	}
}

int inserirRegistro(elementoB **arvB, char* nomeArq, int* reg){
	FILE *arq;
	int tamanhoRegistro,i,j;
	char registro[1024];
	char resto[1024];
	char pk[1024];

	tamanhoRegistro = verificarFixo(nomeArq);
	arq=fopen(nomeArq, "a");

	printf("\n\n====================================\n==========Inserir Registro==========\n====================================\n\n");
	
	printf("Digite a chave primaria com 4 digitos:\n>");
	scanf("%s", registro);
	getchar();
	
	if(strlen(registro)!=4){
		printf("Incorreto\n");
		return 0;
	}
	strcpy(pk,registro);
	registro[4]=' ';


	printf("Digite o resto do registro:\n>");
	scanf("%[^\n]s", resto);
	getchar();

	strcpy(registro, strcat(registro,resto));
	j = strlen(registro);

	for ( i = 0; i < tamanhoRegistro-1; ++i){
		if (j>0){
			putc(registro[i],arq);
			j--;
		} else {
			fputs(" ",arq);
		} 
	}
	fputs("\n",arq);

	*reg = *reg+1;

	fclose(arq);

	inserirNaArvoreB(arvB, pk, reg);
}

int acharRegistro(elementoB **arvB, char* nomeArq){
	FILE *arq;
	char chave[5];
	char leitor[1024];
	int reg,i,seeks;

	arq=fopen(nomeArq, "r");

	printf("\n\n=====================================\n==========Procurar Registro==========\n=====================================\n\n");

	printf("Digite a chave que deseja buscar com 4 digitos:\n>");
	scanf("%s", chave);
	getchar();
	
	if(strlen(chave)!=4){
		printf("Incorreto\n");
		return 0;
	}

	seeks = 1;

	buscarRegistro(arvB, chave, &reg, &seeks);
	
	i=0;
	if (reg!=-1){
		while(fgets(leitor,1024,arq)){
			if (reg==i){
				printf("\n=========REGISTRO ENCONTRADO=========\nSEEKS NECESSARIOS: %d\nREGISTRO: %s\n", seeks, leitor);
				printf("Pression Enter para continuar\n");
				getchar();
				break;
			}
			i++;
		}
	}

	fclose(arq);
}

void buscarRegistro(elementoB **arvB, char* chave, int* reg, int* seeks){
	int i=0;

	while( (i < (*arvB)->numElementos) && (strcmp(chave,(*arvB)->pk[i])>0) ){
		i++;
	}
	if ( (i < (*arvB)->numElementos) && (strcmp(chave,(*arvB)->pk[i])==0) ){
		*reg = (*arvB)->prr[i];
	} else if((*arvB)->folha==true){
		printf("O Registro procurado nao existe\n");
		*reg = -1;
	} else {
		*seeks = *seeks + 1;
		buscarRegistro( &((*arvB)->filho[i]), chave, reg, seeks);
	}
}