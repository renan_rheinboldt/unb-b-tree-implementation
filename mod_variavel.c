#include "interface_variavel.h"

void inicializarArvoreBvariavel(elementoB **arvB, char* nomeArq, int *numRegistro){
	FILE *arq;
	char leitor[1024];
	char pk[1024];
	int i;

	arq=fopen(nomeArq,"r");

	/*Iicializando Árvore-B*/
	fgets(leitor,1024,arq);
	for (i = 0; i < 7; ++i){
		pk[i]=leitor[i];
	}
	criarArvoreB(arvB,pk,numRegistro);
	
	//Inserindo outros elementos na ÁrvoreB
	
	while(fgets(leitor,1024,arq)){
		//printf("WHILE\n");
		*numRegistro += 1;
		for (i = 0; i < 7; ++i){
			//printf("FOR\n");
			//printf("%s\n=============================", pk );
			pk[i]=leitor[i];
		}
		inserirNaArvoreB(arvB, pk, numRegistro);
	}

	fclose(arq);
}

int inserirRegistroVariavel(elementoB **arvB, char* nomeArq, int* reg){
	FILE *arq;
	int tamanhoRegistro,i,j;
	char registro[1024];
	char resto[1024];
	char pk[1024];

	printf("\n\n====================================\n==========Inserir Registro==========\n====================================\n\n");

	tamanhoRegistro = verificarFixo(nomeArq);
	arq=fopen(nomeArq, "a");
	
	printf("Digite a chave primária com 7 digitos:\n>");
	scanf("%s", registro);
	getchar();
	
	if(strlen(registro)!=7){
		printf("Incorreto\n");
		return 0;
	}
	strcpy(pk,registro);
	registro[7]=';';


	printf("Digite o resto do registro:\n>");
	scanf("%[^\n]s", resto);
	getchar();

	strcpy(registro, strcat(registro,resto));
	j = strlen(registro);

	for ( i = 0; i < j; ++i){
		putc(registro[i],arq);
	}
	fputs("\n",arq);

	*reg = *reg+1;

	fclose(arq);

	inserirNaArvoreB(arvB, pk, reg);
}

int acharRegistroVariavel(elementoB **arvB, char* nomeArq){
	FILE *arq;
	char chave[8];
	char leitor[1024];
	int reg,i,seeks;

	arq=fopen(nomeArq, "r");

	printf("\n\n=====================================\n==========Procurar Registro==========\n=====================================\n\n");
	printf("Digite a chave que deseja buscar com 7 digitos:\n>");
	scanf("%s", chave);
	getchar();
	
	if(strlen(chave)!=7){
		printf("Incorreto\n");
		return 0;
	}

	buscarRegistro(arvB, chave, &reg, &seeks);
	
	i=0;
	if (reg!=-1){
		while(fgets(leitor,1024,arq)){
			if (reg==i){
				printf("\n=========REGISTRO ENCONTRADO=========\nSEEKS NECESSARIOS: %d\nREGISTRO: %s\n", seeks, leitor);
				printf("Pression Enter para continuar\n");
				getchar();
				break;
			}
			i++;
		}
	}

	fclose(arq);
}