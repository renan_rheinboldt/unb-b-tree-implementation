all: build clean

build: main.o mod_fixo.o mod_variavel.o
	gcc main.o mod_fixo.o mod_variavel.o -o exe

main.o: main.c main.h arvoreB.h
	gcc -c main.c

mod_fixo.o: mod_fixo.c interface_fixo.h arvoreB.h
	gcc -c  mod_fixo.c

mod_veriavel.o: mod_variavel.c interface_variavel.h arvoreB.h interface_fixo.h
	gcc -c  mod_variavel.c

clean: 
	rm *.o